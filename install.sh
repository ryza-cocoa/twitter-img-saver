#/bin/sh

function error {
    msg=$1;
    echo "[ERROR] ${msg}"
}

function info {
    msg=$1;
    echo "[INFO] ${msg}"
}

function download {
    url=$1
    save_at=$2
    
    CURL=`which curl`
    if [ ${#CURL} -eq 0 ]; then
        error "Cannot find curl, give up"
        exit
    fi
    
    curl -fSsL ${url} -o ${save_at}
}

function yes_or_no {
    read ans
    case $ans in
        [yY])
            echo 1
        ;;
        *)
            echo 0
        ;;
    esac
}

function non_empty_ans {
    while true; do
        read ans
        if [ ${#ans} -gt 0 ]; then
            echo ${ans}
            break
        fi
    done
}

function ans_with_default {
    d=$1
    read ans
    if [ ${#ans} -gt 0 ]; then
        echo $ans
    else
        echo $d
    fi
}

OS_TYPE=""
ARCH="x86_64"
PRE_COMPILED="twitter-img-saver"
case `uname` in
    [Ll]inux)
        OS_TYPE="linux"
        CUR_ARCH=`uname -i`
        if [ $CUR_ARCH != "x86_64" ]; then
            error "Currently only has x86_64 precompiled"
            exit
        fi
        PRE_COMPILED="${PRE_COMPILED}-${OS_TYPE}-${CUR_ARCH}"
    ;;
    [Dd]arwin)
        OS_TYPE="macos"
        PRE_COMPILED="${PRE_COMPILED}-${OS_TYPE}-${ARCH}"
    ;;
    *)
        error "Currently no support for your system"
        exit
    ;;
esac

PROJECT_NAME="twitter-img-saver"
download "${OWO_RESOURCES_ROOT}/${PRE_COMPILED}?ver=${OWO_RESOURCES_VER}" "/usr/local/bin/${PROJECT_NAME}"
if [ $? -eq 0 ]; then
    chmod +x /usr/local/bin/${PROJECT_NAME}
    info "Successfully saved ${PROJECT_NAME} binary to /usr/local/bin/${PROJECT_NAME}"
    
    SYSTEMCTL=`which systemctl`
    if [ ${#SYSTEMCTL} -ne 0 ]; then
        echo -n "Would you like to download systemd service file?[yN]: "
        ans=$( yes_or_no )
        if [ $ans -eq 0 ]; then
            info "Found systemctl, will download ${PROJECT_NAME}.service to /etc/systemd/system/${PROJECT_NAME}.service."
            download "${OWO_RESOURCES_ROOT}/${PROJECT_NAME}.service?ver=${OWO_RESOURCES_VER}" "/etc/systemd/system/${PROJECT_NAME}.service"
            if [ $? -eq 0 ]; then
                info "Successfully saved systemd service file to /etc/systemd/system/${PROJECT_NAME}.service"
            else
                error "Cannot download or save systemd service file"
            fi
        fi
    fi

    echo -n "Would you like to config ${PROJECT_NAME} now? [yN] "
    ans=$( yes_or_no )
    if [ $ans -eq 1 ]; then
        echo -n "Consumer Key: "
        key=$(non_empty_ans)
        
        echo -n "Consumer Secret: "
        secret=$(non_empty_ans)
        
        echo "Images save to: (default working dir is /usr/local/bin if you're using relative path)"
        img_dir=$(non_empty_ans)
        
        echo -n "Would you like to add any Twitter account now? [yN] "
        ans=$( yes_or_no )
        if [ $ans -eq 1 ]; then
            echo -n "username: (without @)"
            username=$(non_empty_ans)
            userList=("${username}")
            while true ; do
                echo -n "Would you like to add more Twitter account now? [yN] "
                ans=$( yes_or_no )
                if [ $ans -eq 1 ]; then
                    echo -n "username: (without @)"
                    username=$(non_empty_ans)
                    userList+=("${username}")
                else
                    break;
                fi
            done
        fi
        users=""
        for value in "${userList[@]}"
        do
            info "adding ${value} to the list"
            users="\"${value}\",\n${users}"
        done
        users=${users:0:$((${#users} - 3))}
        users=`echo -e ${users}`
        cat <<EOF | tee "${HOME}/.twitter-img-saver.json"
{
    "key": "$key",
    "secret": "${secret}",
    "img_dir": "${img_dir}",
    "users": [
        ${users}
    ]
}
EOF
    else
        echo -n "Would you like to download an example config file? [yN] "
        ans=$( yes_or_no )
        if [ $ans -eq 1 ]; then
            overwrite=1
            if [ -e "~/.twitter-img-saver.json" ]; then
                echo -n "You already has a config file, overwrite? [yN] "
                ans=$( yes_or_no )
                overwrite=$ans
            fi
            if [ $overwrite -eq 1 ]; then
                info "~/.twitter-img-saver.json not exists, will download an example config file to ~/.twitter-img-saver.json"
                download "${OWO_RESOURCES_ROOT}/twitter-img-saver.json?ver=${OWO_RESOURCES_VER}" "~/.twitter-img-saver.json"
                if [ $? -eq 0 ]; then
                    info "Successfully saved example config file to ~/.twitter-img-saver.json"
                else
                    error "Cannot download or save example config file"
                fi
            fi
        fi
    fi
fi
