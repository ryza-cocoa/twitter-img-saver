# twitter-img-saver

Save Twitter images from accounts that you interested in.

![screenshot](screenshot.png)

## Installation

There will be a detailed prompt while installation.
```bash
# this script will save twitter-img-saver to /usr/local/bin/twitter-img-saver
# and create a example config file at ~/.twitter-img-saver.json
# and generate a systemd service file at /etc/systemd/system/twitter-img-saver.service if your system is Ubuntu/Debian
bash <(curl -sSLf https://owo.ryza.moe/twitter-img-saver/install.sh)
```

### [Optional] Manually install twitter-img-saver

Firstly, download precompiled binary or manually compile this project. The following content will assume that `twitter-img-saver` is placed at `/usr/local/bin` (or any other location in $PATH).

Then copy `twitter-img-saver.example.json` to `~/.twitter-img-saver.json`, also make sure it's readable by `twitter-img-saver`.

## Config

Edit `~/.twitter-img-saver.json`. 
```json
{
    "key": "KEY",                      # Twitter Consumer Key
    "secret": "SECRET",                # Twitter Consumer Secret
    "img_dir": "/var/www/twitter-imgs" # Directory that you'd like to save these images
    "users": [                         # List of Twitter username
        "username1",
        "username2",
        "username3"
    ]
}
```

## Start Twitter Image Saver
```bash
# Ubuntu / Debian
sudo systemctl enable twitter-img-saver.service
sudo systemctl start twitter-img-saver.service
